# Command Line Interface tool for tracking and visualizing (sleep) data

This repository hosts scripts for tracking and visualizing data. The interface has sleep data specifically in mind but the scripts can be easily altered to show other data, metrics, and different kinds of plots.
The scripts are written in Julia.

### Packages

Make sure to have the `CSV`, `Statistics`, `UnicodePlots` Julia packages installed before running the scripts.

### Execution

Running
```
julia tracking-sleep.jl
```
will prompt you to input a number which, along with the current date, will be appended to the `data.txt` file. This is the file where all of the data resides and it is used for visualization. A sample `data.txt` file has been aded to the repository.

Executing 
```
julia plotting-sleep.jl
```
will visualize the sleep data that was read from `data.txt` and output a random fact from `sleep-facts.txt`. The sleep facts have been collected from https://www.thegoodbody.com/sleep-facts/#Whos-getting-the-most-sleep .

### Preview

This tool was written with the following routine in mind:
1. Boot up your system.
2. A terminal window appears running the `tracking-data.jl` script.
3. A number is entered which is appended to `data.txt`.
4. Another terminal window executes `plotting-sleep.jl` which outputs the plots for the recent (sleep) data.

This routine is relatively easy to set up with a tiling window manager (i3, bspwm). However it could be achieved with different desktop managers by making terminal windows appear at (for example) bootup.

Example `plotting-sleep.jl` output:

![Example](./Screenshots/example1.jpg)

cli-sleep-tracker-visualizer used in context with i3 window manager:

![Example](./Screenshots/example2.jpg)
