using CSV

current_dir = pwd()

df = CSV.read(string(current_dir, "/data.txt"))

# take the latest 7 days (weekly average)
week_data  = df[size(df)[1]-6:size(df)[1], [:date,:sleep_hrs]]
# take the latest 30 days (monthly average)
if size(df)[1] >= 30
	month_data = df[size(df)[1]-29:size(df)[1], [:date,:sleep_hrs]]
else 
	month_data = df[1:size(df)[1], [:date,:sleep_hrs]]	
end

using UnicodePlots

p = barplot(week_data[:,1], week_data[:,2])
title!(p,"Sleep this week")
xlabel!(p,"Hours slept")
ylabel!(p,"Days")
display(p)

println()

using Statistics

avg_week = round(mean(week_data[:,2]), sigdigits=2)
avg_month = round(mean(month_data[:,2]), sigdigits=2)
println("Average hours slept this week: ", avg_week)
println("Average hours slept this month: ", avg_month)

print("Message about your sleep: ")
if avg_week < 6
	println("Your sleep average is abysmal! Please take care of yourself and sleep more!")
elseif avg_week < 7
	println("You still lack a bit of sleep. Please don't forget to sleep more!")
else
	println("You're doing great! Keep sleeping this well!")
end

include(string(current_dir, "/random-facts.jl"))

print("Random sleep fact: ")
print_random_fact()
